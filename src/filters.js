import Vue from 'vue'
import _ from 'lodash'

Vue.filter('formatDate', function (value) {
    if (value) {
        return this.$moment(String(value)).format('MMMM DD, YYYY')
    }
})

Vue.filter('capitalize', function (value) {
    if (!value) return '-'
    value = value.toString()
    if (value == 'not-valid') {
        value = 'invalid'
    }
    if (value == 'n/a') {
        return value.toUpperCase()
    }
    if (value == 'sanction') {
        value = 'sanctioned'
    }
    return value.charAt(0).toUpperCase() + value.slice(1).replace(/_/g, ' ')
})

Vue.filter('booleanText', function (value) {
    let val = 'No'
    if (value) {
        val = 'Yes'
    }
    return val
})

Vue.filter('requireText', function (value) {
    let val = 'Not Required'
    if (value) {
        val = 'Required'
    }
    return val
})

Vue.filter('titleCase', function (value) {
    if (value) {
        return _.startCase(value)
    }
})
