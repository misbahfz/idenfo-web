import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        /*404 Page*/
        {
            name: '/404',
            path: '*',
            components: {
                default: () =>
                    import(
                        /* webpackChunkName: "404" */ '@/views/404/Main.vue'
                    ),
            },

            meta: {
                title: '404 Not Found',
                bodyClass: 'not-found-page',
                showSideBar: false,
                noTopHeader: true,
            },
        },
        {
            path: '/',
            name: 'login-page',
            component: () =>
                import(/* webpackChunkName: "login" */ '@/views/auth/Main.vue'),

            children: [
                {
                    path: '/',
                    name: 'login-page',
                    component: () =>
                        import(
                            /* webpackChunkName: "login" */ '@/views/auth/Login.vue'
                        ),
                    meta: {
                        title: 'Login',
                        showSideBar: false,
                        bodyClass: 'auth-body',
                        requiresGuest: true,
                        noTopHeader: true,
                    },
                },
                {
                    path: 'users/forgot-password',
                    name: 'forgot-page',
                    component: () =>
                        import(
                            /* webpackChunkName: "forgot-password" */ '@/components/auth/Main.vue'
                        ),
                    meta: {
                        title: 'Forgot Password',
                        showSideBar: false,
                        bodyClass: 'auth-body',
                        requiresGuest: true,
                    },
                },
                {
                    path: 'users/reset-password/:token',
                    name: 'reset-password-page',
                    component: () =>
                        import(
                            /* webpackChunkName: "reset-password" */ '@/views/auth/ResetPassword.vue'
                        ),
                    meta: {
                        title: 'Reset Password',
                        showSideBar: false,
                        noFooterBar: true,
                        bodyClass: 'auth-body',
                        requiresGuest: true,
                    },
                },
                {
                    path: 'users/create-password/:token',
                    name: 'create-password-page',
                    component: () =>
                        import(
                            /* webpackChunkName: "reset-password" */ '@/views/auth/CreatePassword.vue'
                        ),
                    meta: {
                        title: 'Create Password',
                        showSideBar: false,
                        noFooterBar: true,
                        bodyClass: 'auth-body',
                        requiresGuest: true,
                    },
                },
                {
                    path: '/signup',
                    component: () =>
                        import(
                            /* webpackChunkName: "reset-password" */ '@/views/auth/CustomerSignup.vue'
                        ),
                    meta: {
                        title: 'Sign Up',
                        showSideBar: false,
                        bodyClass: 'auth-body',
                        requiresGuest: true,
                        noTopHeader: true,
                    },
                },
            ],
        },
        {
            path: '/billing',
            name: 'billing',
            component: () =>
                import(
                    /* webpackChunkName: "reset-password" */ '@/views/billing/Main.vue'
                ),
            meta: {
                title: 'Accounts Dashboard',
                showSideBar: true,
                requiresAuth: true,
                permission: ['register-customer'],
            },
            children: [
                {
                    path: '/',
                    name: 'accounts',
                    component: () =>
                        import(
                            /* webpackChunkName: "reset-password" */ '@/views/billing/Packages.vue'
                        ),
                    meta: {
                        showSideBar: true,
                        bodyClass: 'register-customer-body',
                        name: 'Accounts',
                        title: 'Accounts',
                        requiresAuth: true,
                        permission: ['register-customer'],
                    },
                    requiresAuth: true,
                },
                {
                    path: '/success',
                    name: 'success',
                    component: () =>
                        import(
                            /* webpackChunkName: "reset-password" */ '@/views/billing/Success.vue'
                        ),
                    meta: {
                        showSideBar: true,
                        bodyClass: 'register-customer-body',
                        name: 'Checkout Successful',
                        title: 'Checkout Successful',
                        requiresAuth: true,
                        permission: ['register-customer'],
                    },
                    requiresAuth: true,
                },
                {
                    path: '/cancelled',
                    name: 'cancelled',
                    component: () =>
                        import(
                            /* webpackChunkName: "reset-password" */ '@/views/billing/Cancel.vue'
                        ),
                    meta: {
                        showSideBar: true,
                        bodyClass: 'register-customer-body',
                        name: 'Checkout Cancelled',
                        title: 'Checkout Cancelled',
                        requiresAuth: true,
                        permission: ['register-customer'],
                    },
                    requiresAuth: true,
                },
            ],
        },
        {
            path: '/contact-us',
            name: 'contact-us-page',
            component: () =>
                import(
                    /* webpackChunkName: "contact-us" */ '@/views/ContactUs.vue'
                ),
            meta: {
                title: 'Contact Us',
                showSideBar: false,
                bodyClass: 'auth-body',
                requiresGuest: true,
            },
        },
        {
            path: '/insights',
            name: 'dashboard',
            component: () =>
                import(
                    /* webpackChunkName: "view-alerts" */ '@/views/insights/Main.vue'
                ),
            children: [
                {
                    path: '/',
                    name: 'view-activity-dashboard',
                    component: () =>
                        import(
                            /* webpackChunkName: "dashboard-activity" */ '@/views/insights/Activity.vue'
                        ),
                    meta: {
                        title: 'Insights | Activity Dashboard',
                        requiresAuth: true,
                        showSideBar: true,
                        permission: ['view-activity-dashboard'],
                    },
                },
                {
                    path: 'activity-dashboard',
                    name: 'view-activity-dashboard',
                    component: () =>
                        import(
                            /* webpackChunkName: "dashboard-activity" */ '@/views/insights/Activity.vue'
                        ),
                    meta: {
                        title: 'Insights | Activity Dashboard',
                        requiresAuth: true,
                        showSideBar: true,
                        permission: ['view-activity-dashboard'],
                    },
                },
                {
                    path: 'alerts',
                    name: 'view-alerts',
                    component: () =>
                        import(
                            /* webpackChunkName: "view-alerts" */ '@/views/insights/Alert.vue'
                        ),
                    meta: {
                        title: 'Insights | Alerts',
                        requiresAuth: true,
                        showSideBar: true,
                        permission: ['view-alerts'],
                    },
                },
                {
                    path: 'customer-statistics',
                    name: 'view-customer-statistics',
                    component: () =>
                        import(
                            /* webpackChunkName: "dashboard-reporting" */ '@/views/insights/Statistics.vue'
                        ),
                    meta: {
                        title: 'Insights | Customer Statistics',
                        requiresAuth: true,
                        showSideBar: true,
                        permission: ['view-customer-statistics'],
                    },
                },
            ],
        },
        {
            path: '/users',
            name: 'systems',
            component: () =>
                import(
                    /* webpackChunkName: "system-users" */ '@/views/system-users/Main.vue'
                ),
            children: [
                {
                    path: '/',
                    name: 'view-users',
                    component: () =>
                        import(
                            /* webpackChunkName: "system-users" */ '@/views/system-users/Users.vue'
                        ),
                    meta: {
                        title: 'Users',
                        requiresAuth: true,
                        showSideBar: true,
                        permission: ['view-users'],
                    },
                },
                {
                    path: 'users',
                    name: 'view-users',
                    component: () =>
                        import(
                            /* webpackChunkName: "system-users" */ '@/views/system-users/Users.vue'
                        ),
                    meta: {
                        title: 'Users',
                        showSideBar: true,
                        requiresAuth: true,
                        permission: ['view-users'],
                    },
                },
                {
                    path: 'roles',
                    name: 'view-roles',
                    component: () =>
                        import(
                            /* webpackChunkName: "roles" */ '@/views/system-users/Roles.vue'
                        ),
                    meta: {
                        title: 'Users | Roles & Permission',
                        requiresAuth: true,
                        showSideBar: true,
                        permission: ['view-roles'],
                    },
                },
                {
                    path: 'data-segments',
                    name: 'view-data-segments',
                    component: () =>
                        import(
                            /* webpackChunkName: "branches" */ '@/views/system-users/DataSegments.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Users | Data Segments',
                        requiresAuth: true,
                        permission: ['view-data-segments'],
                    },
                },
                {
                    path: 'application-access',
                    name: 'view-application-access',
                    component: () =>
                        import(
                            /* webpackChunkName: "access" */ '@/views/system-users/Access.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'System Users | Application Access',
                        requiresAuth: true,
                        permission: ['view-application-access'],
                    },
                },
            ],
        },
        {
            path: '/system-logs',
            name: 'system-logs',
            component: () =>
                import(
                    /* webpackChunkName: "system-logs" */ '@/views/system-logs/Main.vue'
                ),
            meta: {
                showSideBar: true,

                title: 'System Logs',
                requiresAuth: true,
                permission: ['system-logs'],
            },
        },

        {
            path: '/download/:folder/:type/:fileName',
            name: 'download',
            component: () => import('@/views/download/Download.vue'),
            meta: {
                showSideBar: true,

                title: 'Download File',
                requiresAuth: true,
                permission: [],
            },
        },

        {
            path: '/customer-profiles',
            name: 'profile',

            component: () => import('./views/customer-profiles/Main.vue'),

            children: [
                {
                    path: '/',
                    name: 'view-customer-profile',
                    component: () =>
                        import('./views/customer-profiles/AllCustomer.vue'),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Profile | All Customers',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
                {
                    path: '/customer-profiles/temp-customers',
                    name: 'view-temp-customer-profile',
                    component: () =>
                        import(
                            './views/customer-profiles/AllTempCustomers.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Temp Customer Profile | All Customers',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
                {
                    path: '/customer-profiles/all-customer',
                    name: 'view-customer-profile',
                    component: () =>
                        import('./views/customer-profiles/AllCustomer.vue'),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Profile | All Customers',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },

                {
                    path: '/customer-profiles/advance-search',
                    name: 'advance-search',

                    component: () =>
                        import('./views/customer-profiles/AdvanceSearch.vue'),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Profile | Advance Search',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
            ],
        },

        {
            path: '/customer-profiles/temp-customer-information/:id',
            name: 'temp-customer-details',
            props: true,
            component: () =>
                import(
                    './views/customer-profiles/profile-details/MainTemp.vue'
                ),
            children: [
                {
                    path: '/',
                    name: 'temp-customer-information',
                    props: true,
                    component: () =>
                        import(
                            './views/customer-profiles/profile-details/TempCustomerInformation.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Information | Customer Profile',
                        name: 'Customer Profile',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
            ],
        },

        {
            path: '/customer-profiles/customer-information/:id',
            name: 'customer-details',
            props: true,
            component: () =>
                import('./views/customer-profiles/profile-details/Main.vue'),
            children: [
                {
                    path: '/customer-profiles/customer-information/:id',
                    name: 'customer-information',
                    props: true,
                    component: () =>
                        import(
                            './views/customer-profiles/profile-details/CustomerInformation.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Information | Customer Profile',
                        name: 'Customer Profile',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
                {
                    path: '/customer-profiles/name-screening/:id',
                    name: 'screening-customer',
                    props: true,
                    component: () =>
                        import(
                            './views/customer-profiles/profile-details/ScreeningCustomer.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Information | Name Screening',
                        name: 'Name Screening',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },

                {
                    path: '/customer-profiles/document-verification/:id',
                    name: 'document-verification',
                    props: true,
                    component: () =>
                        import(
                            './views/customer-profiles/profile-details/DocumentVerification.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Information | Document Verification',
                        name: 'Document Verification',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
                {
                    path: '/customer-profiles/kyc-risk-rating/:id',
                    name: 'kyc-risk-rating',
                    props: true,
                    component: () =>
                        import(
                            './views/customer-profiles/profile-details/KYCRiskRating.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Information | KYC Risk Rating',
                        name: 'KYC Risk Rating',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
                {
                    path: '/customer-profiles/activity-timeline/:id',
                    name: 'activity-timeline',
                    props: true,
                    component: () =>
                        import(
                            './views/customer-profiles/profile-details/ActivityTimeline.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Customer Information | Activity Timeline',
                        name: 'Activity Timeline',
                        requiresAuth: true,
                        permission: ['view-customer-profile'],
                    },
                },
            ],
        },
        {
            path: '/configuration',
            name: 'configuration',

            component: () =>
                import(
                    /* webpackChunkName: "configuration" */ '@/views/configuration/Main.vue'
                ),
            children: [
                {
                    path: '/',
                    name: 'idenfo-engine',
                    component: () =>
                        import(
                            /* webpackChunkName: "idenfo-engine" */ './views/configuration/idenfo-engine/Main.vue'
                        ),
                    children: [
                        {
                            path: '/',
                            name: 'idenfo-engine',
                            component: () =>
                                import(
                                    /* webpackChunkName: "nationality" */ './views/configuration/idenfo-engine/RiskFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Risk Factor & Weightage',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/risk',
                            name: 'conf-risk',
                            component: () =>
                                import(
                                    /* webpackChunkName: "risk-factor" */ './views/configuration/idenfo-engine/RiskFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Risk Factor & Weightage',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/',
                            name: 'idenfo-engine',
                            component: () =>
                                import(
                                    /* webpackChunkName: "nationality" */ './views/configuration/idenfo-engine/RiskFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Risk Factor & Weightage',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/risk-rating',
                            name: 'risk-rating',
                            component: () =>
                                import(
                                    /* webpackChunkName: "risk-rating" */ './views/configuration/idenfo-engine/RiskRating.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Risk Rating Score & Risk Rating Overrides',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/document-verification-factor',
                            name: 'document-verification-factor',
                            component: () =>
                                import(
                                    /* webpackChunkName: "document-verification-factor" */ './views/configuration/idenfo-engine/DocumentVerificationFactors.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Document Verification Factors',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },

                        {
                            path: '/configuration/idenfo-engine/name-screen-score',
                            name: 'name-screen-score',
                            component: () =>
                                import(
                                    /* webpackChunkName: "name-screen-score" */ './views/configuration/idenfo-engine/NameScreenScore.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Name Screening Score Factors',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/nationality',
                            name: 'conf-nationality',
                            component: () =>
                                import(
                                    /* webpackChunkName: "nationality" */ './views/configuration/idenfo-engine/Nationality.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Nationality Factor',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/country',
                            name: 'conf-country',
                            component: () =>
                                import(
                                    /* webpackChunkName: "nationality" */ './views/configuration/idenfo-engine/Country.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Country of Residence Factor',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/state',
                            name: 'state-factor',
                            component: () =>
                                import(
                                    /* webpackChunkName: "state" */ './views/configuration/idenfo-engine/StateFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | State Factor',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/work',
                            name: 'work-factor',
                            component: () =>
                                import(
                                    /* webpackChunkName: "work" */ './views/configuration/idenfo-engine/WorkFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Work Type Factor',
                                requiresAuth: true,
                                innerTabName: 'Idenfo Engine',
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/industry',
                            name: 'industry-factor',
                            component: () =>
                                import(
                                    /* webpackChunkName: "industry" */ './views/configuration/idenfo-engine/IndustryFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Industry Factor',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/product',
                            name: 'productFactor',
                            component: () =>
                                import(
                                    /* webpackChunkName: "productFactor" */ './views/configuration/idenfo-engine/ProductFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Product Factor',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/relationship',
                            name: 'relationship-factor',
                            component: () =>
                                import(
                                    /* webpackChunkName: "relationship-factor" */ './views/configuration/idenfo-engine/RelationshipPanel.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Relationship Length Factor',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/channel',
                            name: 'channel-factor',
                            component: () =>
                                import(
                                    /* webpackChunkName: "channel-factor" */ './views/configuration/idenfo-engine/ChannelFactor.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Channel Type Factor',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/purpose-of-action',
                            name: 'purpose-action',
                            component: () =>
                                import(
                                    /* webpackChunkName: "purpose-of-action" */ './views/configuration/idenfo-engine/PurposeAction.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Purpose of Action Management',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/gender',
                            name: 'idenfo-engine-gender',
                            component: () =>
                                import(
                                    /* webpackChunkName: "gender" */ './views/configuration/idenfo-engine/Gender.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Gender',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/risk-level-review',
                            name: 'idenfo-engine-risk-level-review',
                            component: () =>
                                import(
                                    /* webpackChunkName: "risk-level-review" */ './views/configuration/idenfo-engine/RiskLevelReview.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | Risk Level Review Configuration',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/aum',
                            name: 'idenfo-engine-aum',
                            component: () =>
                                import(
                                    './views/configuration/idenfo-engine/AUMConfigurations.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | AUM Configurations',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                        {
                            path: '/configuration/idenfo-engine/sar',
                            name: 'idenfo-engine-sar',
                            component: () =>
                                import(
                                    './views/configuration/idenfo-engine/SARConfigurations.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Idenfo Engine | SAR Configurations',
                                responsiveMenuItem: true,
                                innerTabName: 'Idenfo Engine',
                                requiresAuth: true,
                                permission: ['idenfo-engine'],
                            },
                        },
                    ],
                },
                {
                    path: '/configuration/screening-data/',
                    name: 'view-screening-data',
                    component: () =>
                        import(
                            /* webpackChunkName: "customer" */ './views/configuration/screening-data/Main.vue'
                        ),
                    children: [
                        {
                            path: '/configuration/screening-data/gender',
                            name: 'gender',
                            component: () =>
                                import(
                                    /* webpackChunkName: "screening-data" */ './views/configuration/screening-data/Gender.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Gender',
                                requiresAuth: true,
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/',
                            name: 'gender',
                            component: () =>
                                import(
                                    /* webpackChunkName: "gender" */ './views/configuration/screening-data/Gender.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Gender',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/',
                            name: 'gender',
                            component: () =>
                                import(
                                    /* webpackChunkName: "gender" */ './views/configuration/screening-data/Gender.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Screening Data',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/deceased',
                            name: 'deceased',
                            component: () =>
                                import(
                                    /* webpackChunkName: "deceased" */ './views/configuration/screening-data/Deceased.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Deceased',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/record-status',
                            name: 'record-status',
                            component: () =>
                                import(
                                    /* webpackChunkName: "record-status" */ './views/configuration/screening-data/RecordStatus.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Record Status',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/name-type',
                            name: 'name-type',
                            component: () =>
                                import(
                                    /* webpackChunkName: "name-type" */ './views/configuration/screening-data/NameType.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Name Type',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/descprition-1',
                            name: 'descprition-1',
                            component: () =>
                                import(
                                    /* webpackChunkName: "descprition-1" */ './views/configuration/screening-data/Descprition1.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Description 1',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/descprition-2',
                            name: 'descprition-2',
                            component: () =>
                                import(
                                    /* webpackChunkName: "descprition-2" */ './views/configuration/screening-data/Descprition2.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Description 2',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/descprition-3',
                            name: 'descprition-3',
                            component: () =>
                                import(
                                    /* webpackChunkName: "descprition-3" */ './views/configuration/screening-data/Descprition3.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Description 3',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/role-type',
                            name: 'role-type',
                            component: () =>
                                import(
                                    /* webpackChunkName: "role-type" */ './views/configuration/screening-data/RoleType.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Role Type',
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/occupation',
                            name: 'occupation',
                            component: () =>
                                import(
                                    /* webpackChunkName: "occupation" */ './views/configuration/screening-data/Occupation.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Occupation',
                                responsiveMenuItem: true,
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/date-type',
                            name: 'data-type',
                            component: () =>
                                import(
                                    /* webpackChunkName: "data-type" */ './views/configuration/screening-data/DateType.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Date Type',
                                responsiveMenuItem: true,
                                requiresAuth: true,
                                innerTabName: 'Screening Data',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/sanction-reference',
                            name: 'sanction-reference',
                            component: () =>
                                import(
                                    /* webpackChunkName: "sanction-reference" */ './views/configuration/screening-data/SactionReference.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Sanction Reference',
                                responsiveMenuItem: true,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/country',
                            name: 'screening-country',
                            component: () =>
                                import(
                                    /* webpackChunkName: "country" */ './views/configuration/screening-data/Countries.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Countries',
                                responsiveMenuItem: true,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/country-type',
                            name: 'screening-country-type',
                            component: () =>
                                import(
                                    /* webpackChunkName: "country-type" */ './views/configuration/screening-data/CountryType.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Country Type',
                                responsiveMenuItem: true,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/identification',
                            name: 'screening-indentification',
                            component: () =>
                                import(
                                    /* webpackChunkName: "identification" */ './views/configuration/screening-data/Identification.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Identification',
                                responsiveMenuItem: true,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/relationship-type',
                            name: 'screening-relationship',
                            component: () =>
                                import(
                                    /* webpackChunkName: "relationship-type" */ './views/configuration/screening-data/Relationship.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 0,
                                title: 'Configuration | Screening Data | Relationship Type',
                                responsiveMenuItem: true,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                permission: ['view-screening-data'],
                            },
                        },

                        // world check routes
                        {
                            path: '/configuration/screening-data/world-check',
                            name: 'world-check-category',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-category" */ './views/configuration/screening-data/world-check/Category.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | Category',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/category',
                            name: 'world-check-category',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-category" */ './views/configuration/screening-data/world-check/Category.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | Category',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/sub-category',
                            name: 'world-check-sub-category',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-sub-category" */ './views/configuration/screening-data/world-check/SubCategory.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | Sub-Category',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/deceased',
                            name: 'world-check-deceased',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-deceased" */ './views/configuration/screening-data/world-check/Deceased.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | Deceased',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/location',
                            name: 'world-check-location',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-location" */ './views/configuration/screening-data/world-check/Locations.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | Locations',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/EIType',
                            name: 'world-check-eitype',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-EIType" */ './views/configuration/screening-data/world-check/EIType.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | E/I Type',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/keyword',
                            name: 'world-check-keyword',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-keyword" */ './views/configuration/screening-data/world-check/Keyword.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | Keyword',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/update-category',
                            name: 'world-check-update-category',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-update-category" */ './views/configuration/screening-data/world-check/UpdateCategory.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | Update Category',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/world-check/pep-status',
                            name: 'world-check-pep-status',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-update-category" */ './views/configuration/screening-data/world-check/PepStatus.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 1,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | World Check | PEP Status',
                                permission: ['view-screening-data'],
                            },
                        },
                        {
                            path: '/configuration/screening-data/black-list',
                            name: 'black-list',
                            component: () =>
                                import(
                                    /* webpackChunkName: "world-check-update-category" */ './views/configuration/screening-data/Blacklisted.vue'
                                ),
                            meta: {
                                showSideBar: true,
                                selectedTab: 2,
                                innerTabName: 'Screening Data',
                                requiresAuth: true,
                                title: 'Configuration | Black List',
                                permission: ['view-screening-data'],
                            },
                        },
                    ],
                },
                {
                    path: '/configuration/simulate/',
                    name: 'initiate-simulation-process',
                    component: () =>
                        import(
                            /* webpackChunkName: "simulate" */ './views/configuration/Simulate.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Configuration | Simulation Switch',
                        requiresAuth: true,
                        permission: ['initiate-simulation-process'],
                    },
                },

                {
                    path: '/configuration/form-builder/',
                    name: 'form-builder',
                    component: () =>
                        import('./views/configuration/form-builder/Main.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'form-builder-setup-profile',
                            component: () =>
                                import(
                                    './views/configuration/form-builder/SetupProfile.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Registration Form Builder | Setup Profile',
                                requiresAuth: true,
                                permission: ['form-builder'],
                                formBuilder: 'setup-profile',
                                innerTabName: 'Form Builder',
                            },
                        },
                        {
                            path: '/configuration/form-builder/account-info',
                            name: 'form-builder-account-info',
                            component: () =>
                                import(
                                    './views/configuration/form-builder/SetupProfile.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Registration Form Builder | Account Information',
                                requiresAuth: true,
                                permission: ['form-builder'],
                                formBuilder: 'account-info',
                                innerTabName: 'Form Builder',
                            },
                        },
                        {
                            path: '/configuration/form-builder/preview-form/:name',
                            name: 'preview_form',
                            component: () =>
                                import(
                                    './views/configuration/form-builder/PreviewRegistrationForm.vue'
                                ),
                            meta: {
                                showSideBar: true,

                                title: 'Configuration | Registration Form Builder | Preview Registration Form',
                                requiresAuth: true,
                                previewForm: true,
                                permission: ['form-builder'],
                                innerTabName: 'Form Builder',
                            },
                        },
                    ],
                },

                {
                    path: '/configuration/branding/',
                    name: 'branding',
                    component: () =>
                        import(
                            /* webpackChunkName: "branding" */ './views/configuration/Branding.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Configuration | Branding Configurations',
                        requiresAuth: true,
                        permission: ['branding'],
                    },
                },
                {
                    path: '/configuration/on-boarding-configuration/',
                    name: 'on-boarding-configuration',
                    component: () =>
                        import(
                            /* webpackChunkName: "on-boarding-configuration" */ './views/configuration/OnBoardingCustomer.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        title: 'Configuration | On Boarding Configuration',
                        requiresAuth: true,
                        permission: ['ocr'],
                    },
                },
            ],
        },
        {
            path: '/register-customer',
            name: 'register-customer',
            component: () =>
                import(
                    /* webpackChunkName: "register-customer" */ './views/register-customer/MainOld.vue'
                ),
            children: [
                {
                    path: '/',
                    name: 'setup-profile',
                    component: () =>
                        import(
                            /* webpackChunkName: "upload-document" */ './views/register-customer/SetupProfile.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        name: 'Register Profile',
                        title: 'Register Customer | Setup Profile',
                        sessionStep: '1',
                        bodyClass: 'register-customer-body',
                        requiresAuth: true,
                        permission: ['register-customer'],
                    },
                },
                {
                    path: '/register-customer/setup-profile',
                    name: 'setup-profile',
                    component: () =>
                        import(
                            /* webpackChunkName: "setup-profile" */ './views/register-customer/SetupProfile.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        name: 'Register Profile',
                        title: 'Register Customer | Setup Profile',
                        sessionStep: '2',
                        requiresAuth: true,
                        permission: ['register-customer'],
                    },
                },
                // {
                //     path: '/register-customer/account-info',
                //     name: 'account-info',
                //     component: () =>
                //         import(/* webpackChunkName: "account-info" */ './views/register-customer/AccountInfo.vue'),
                //     meta: {
                //         showSideBar: true,

                //         name: 'Register Customers',
                //         title: 'Register Customer | Account Info',
                //         sessionStep: '3',
                //         requiresAuth: true,
                //         permission: ['register-customer'],
                //     },
                // },
                {
                    path: '/register-customer/finish',
                    name: 'finish',
                    component: () =>
                        import(
                            /* webpackChunkName: "finish" */ './views/register-customer/Finish.vue'
                        ),
                    meta: {
                        showSideBar: true,

                        name: 'Register Profile',
                        title: 'Register Customer | Finish',
                        sessionStep: '4',
                        requiresAuth: true,
                        permission: ['register-customer'],
                    },
                },
            ],
        },

        {
            path: '/:dataSegmentId/register',
            name: 'register-customer',
            props: true,
            component: () =>
                import(
                    /* webpackChunkName: "public-register-customer" */ './views/register-customer/Main.vue'
                ),
            children: [
                {
                    path: '/',
                    name: 'public-registration-upload-document',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-upload-document" */ './views/register-customer/UploadDocuments.vue'
                        ),
                    meta: {
                        name: 'Online Account Opening Application',
                        title: 'Public Registration | Upload Document',
                        sessionStep: '1',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/register/record-video',
                    name: 'public-registration-record-video',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-record-video" */ './views/register-customer/RecordVideo.vue'
                        ),
                    meta: {
                        name: 'Online Account Opening Application',
                        title: 'Public Registration | Record Video',
                        sessionStep: '2',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/register/setup-profile',
                    name: 'public-registration-setup-profile',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-setup-profile" */ './views/register-customer/SetupProfile.vue'
                        ),
                    meta: {
                        name: 'Online Account Opening Application',
                        title: 'Public Registration | Setup Profile',
                        sessionStep: '3',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/register/account-info',
                    name: 'public-registration-account-info',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-account-info" */ './views/register-customer/AccountInfo.vue'
                        ),
                    meta: {
                        name: 'Online Account Opening Application',
                        title: 'Public Registration | Account Info',
                        sessionStep: '4',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/register/preview',
                    name: 'public-registration-preview',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-account-info" */ './views/register-customer/Preview.vue'
                        ),
                    meta: {
                        name: 'Online Account Opening Application',
                        title: 'Public Registration | Account Info',
                        sessionStep: '5',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/register/finish',
                    name: 'public-registration-finish',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "finish" */ './views/register-customer/Finish.vue'
                        ),
                    meta: {
                        name: 'Online Account Opening Application',
                        title: 'Public Registration | Finish',
                        sessionStep: '6',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
            ],
        },

        {
            path: '/:dataSegmentId/:token/deep-register',
            name: 'deep-register-customer',
            props: true,
            component: () =>
                import(
                    /* webpackChunkName: "public-register-customer" */ './views/deep-register-customer/Main.vue'
                ),
            children: [
                {
                    path: '/',
                    name: 'deep-registration-upload-document',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-upload-document" */ './views/deep-register-customer/UploadDocuments.vue'
                        ),
                    meta: {
                        name: 'Online Verification',
                        title: 'Collaborative',
                        sessionStep: '1',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/:token/deep-register/record-video',
                    name: 'deep-registration-record-video',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-record-video" */ './views/deep-register-customer/RecordVideo.vue'
                        ),
                    meta: {
                        name: 'Online Verification',
                        title: 'Collaborative',
                        sessionStep: '2',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/:token/deep-register/setup-profile',
                    name: 'deep-registration-setup-profile',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-setup-profile" */ './views/deep-register-customer/SetupProfile.vue'
                        ),
                    meta: {
                        name: 'Online Verification',
                        title: 'Collaborative',
                        sessionStep: '3',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/:token/deep-register/account-info',
                    name: 'deep-registration-account-info',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-account-info" */ './views/deep-register-customer/AccountInfo.vue'
                        ),
                    meta: {
                        name: 'Online Verification',
                        title: 'Collaborative',
                        sessionStep: '4',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/:token/deep-register/preview',
                    name: 'deep-registration-preview',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "public-registration-account-info" */ './views/deep-register-customer/Preview.vue'
                        ),
                    meta: {
                        name: 'Online Verification',
                        title: 'Collaborative',
                        sessionStep: '5',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
                {
                    path: '/:dataSegmentId/:token/deep-register/finish',
                    name: 'deep-registration-finish',
                    props: true,
                    component: () =>
                        import(
                            /* webpackChunkName: "finish" */ './views/deep-register-customer/Finish.vue'
                        ),
                    meta: {
                        name: 'Online Verification',
                        title: 'Collaborative',
                        sessionStep: '6',
                        showSideBar: false,
                        publicRegistration: true,
                        bodyClass: 'auth-body',
                    },
                },
            ],
        },
    ],
})
