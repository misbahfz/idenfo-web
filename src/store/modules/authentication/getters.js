export default {
    user(state) {
        return state.user
    },
    vueauth_token(state) {
        return state.vueauth_token
    },
    userId(state) {
        return state.userId
    },
    scopes(state) {
        return state.scopes
    },
    hasScopes(state) {
        return (...scopes) => {
            return _.intersection(scopes, state.scopes).length
        }
    },
}
