export default {
    setCustomersData(state, payload) {
        state.customersData = payload
    },
    setCustomerRecord(state, payload) {
        localStorage.setItem('customerRecord', JSON.stringify(payload))
        state.customerRecord = payload
    },
    setRefreshProfile(state, payload) {
        state.refreshProfile = payload
    },
}
