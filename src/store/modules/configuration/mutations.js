export default {
    setConfigurationData(state, payload) {
        state.configurationData = payload
    },

    setLogoLight(state, data) {
        if (data) {
            console.log(data, 'Data in progress logo light')
            state.logoLight = data
        }
    },
    setLogoDark(state, data) {
        if (data) {
            console.log(data, 'Data in progress logo dark')
            state.logoDark = data
        }
    },
}
