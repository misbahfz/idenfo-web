export default {
    logoLight(state) {
        return state.logoLight
    },
    logoDark(state) {
        return state.logoDark
    },
}
