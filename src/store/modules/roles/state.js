export default {
    rolesList: {},
    roleFields: [
        {
            key: 'title',
            label: 'Role Title',
            sortable: true,
        },
        {
            key: 'associatedUsers',
            label: 'Associate Users',
            class: 'text-center',
            sortable: true,
        },
        {
            key: 'status',
            label: 'Status',
            class: 'text-center status-absolute-pos',
            sortable: true,
        },
        {
            key: 'action',
            label: 'Action',
            tdClass: 'user-roles-action',
        },
    ],

    permissions: {
        insights: [
            { text: 'View Alerts', value: 'view-alerts' },
            {
                text: 'View Activity Dashboard',
                value: 'view-activity-dashboard',
            },
            {
                text: 'View Customer Statistics',
                value: 'view-customer-statistics',
            },
            {
                text: 'Export Activity Dashboard',
                value: 'export-activity-dashboard',
            },
            {
                text: 'Export Customer Statistics',
                value: 'export-customer-statistics',
            },
        ],
        customerProfiles: [
            {
                text: 'View Customer Profile',
                value: 'view-customer-profile',
            },
            { text: 'Register Customer', value: 'register-customer' },
            {
                text: 'KYC Status Investigation & Review',
                value: 'kyc-status-review',
            },
            {
                text: 'Manually Overwrite Verification Status',
                value: 'manually-overwrite',
            },
            { text: 'Open Alert Against Customers', value: 'open-alerts' },
            { text: 'Export Customers', value: 'export-customer-profile' },
            {
                text: 'Export Activity Timeline',
                value: 'export-activity-timeline',
            },
        ],

        configurations: [
            { text: 'Manage Idenfo Engine', value: 'idenfo-engine' },
            {
                text: 'Manage View Screening Data',
                value: 'view-screening-data',
            },
            { text: 'Manage Form Builder', value: 'form-builder' },
            {
                text: 'Initiate Simulation Process',
                value: 'initiate-simulation-process',
            },
            { text: 'Manage Branding', value: 'branding' },
            { text: 'Manage OCR', value: 'ocr' },
        ],

        systemUser: [
            { text: 'View System Users', value: 'view-system-users' },
            { text: 'Manage System Users', value: 'manage-system-users' },
            { text: 'View Roles & Permission', value: 'view-roles' },
            { text: 'Manage Roles & Permission', value: 'manage-roles' },
            { text: 'View Data Segments', value: 'view-data-segments' },
            { text: 'Manage Data Segments', value: 'manage-data-segments' },
            {
                text: 'View Application Access',
                value: 'view-application-access',
            },
            {
                text: 'Manage Application Access',
                value: 'manage-application-access',
            },
        ],

        systemLogs: [{ text: 'View System Logs', value: 'system-logs' }],
        statusReview: [
            { text: 'Maker', value: 'maker' },
            {
                text: 'Checker',
                value: 'checker',
            },
            {
                text: 'Approver',
                value: 'approver',
            },
            {
                text: 'Unlock Alerts',
                value: 'unlock-alerts',
            },
        ],
    },
}
