export default {
    applicationAccessList: function (state) {
        return state.applicationAccessList
    },
    applicationAccessFields: function (state) {
        return state.applicationAccessFields
    },
}
