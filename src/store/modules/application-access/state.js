export default {
    applicationAccessList: {},
    applicationAccessFields: [
        {
            key: 'title',
            label: 'Application Title',
            sortable: true,
        },
        {
            key: 'dataSegmentIds',
            label: 'Data Segments',
            sortable: true,
        },
        {
            key: 'status',
            label: 'Status',
            sortable: true,
            class: 'text-center status-absolute-pos',
        },
        {
            key: 'action',
            label: 'Action',
            tdClass: 'user-roles-action',
        },
    ],
}
