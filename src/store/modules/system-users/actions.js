const apiUrl = process.env.VUE_APP_API_URL
import axios from 'axios'
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}
export default {
    async customerSignUp({ commit }, payload) {
        commit('setLoading', true)
        try {
            let { data } = await axios.post(apiUrl + 'users/', payload, headers)
            console.log(data)
            commit('setSuccess', data.message)
            commit('reloadList', true)
            return data
        } catch (errors) {
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
    async updateSystemUser({ commit }, payload) {
        commit('setLoading', true)
        commit('showPageLoader', true)
        try {
            let { data } = await axios.put(
                apiUrl + 'users/' + payload.id,
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit('setLoading', false)
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
        commit('showPageLoader', false)
    },
    /*    async getAllUsers({ commit }) {
        try {
            let { data } = await axios.get(apiUrl + 'system-users/users', {
                params: {
                    perPage: 10,
                },
                headers: {
                    Authorization: `Bearer ${localStorage.getItem(
                        'vueauth_token'
                    )}`,
                },
            })
            await commit('systemUsersDataList', data)
        } catch (errors) {
            commit('setLoading', false)
            commit('setError', errors)
        }
    },
*/
}
