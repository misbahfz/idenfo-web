export default {
    alertsFilteredData: (state) => state.alertsFilteredData,
    chartsHeadings: (state) => state.chartsHeadings,
}
