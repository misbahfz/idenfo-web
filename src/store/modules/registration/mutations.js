import _ from 'lodash'
import Vue from 'vue'

export default {
    initDefaultFields(state, key) {
        Vue.set(state, key, {
            idenfoId: null,
            dataSegmentId: null,
            basicInfo: {
                token: '',
                firstName: null,
                middleName: '',
                lastName: '',
                genderId: null,
                dateOfBirth: null,
                nationalityId: null,
                nationality: null,
                countryOfResidenceId: null,
                countryOfResidence: null,
                profileType: null,
                gender: null
            },
            identityInfo: {
                type: null,
                number: null,
                expiryDate: null,
                isLifetimeExpiry: null
            },
            occupationInfo: {
                workTypeId: null,
                industryId: null,
                taxId: null,
                industry: null,
                workType: null
            },
            pepDeclaration: {
                anyPublicPos: null,
                posLastTwoMonths: null,
                everPublicPosition: null,
                diplomaticImmunity: null,
                relativeTwelveMonths: null,
                closeAssociateTwelveMonths: null,
                conviction: null,
                other: null,
            },
            sourceOfWealth: {
                evidenced: null,
                OtherEvidenced: null,
            },
            sourceOfFunds: {
                evidenced: null,
                OtherEvidenced: null,
            },

            contactInfo: {
                address: null,
                stateId: null,
                state: null,
                city: null,
                postalCode: null,
                isdCode: null,
                phoneNumber: null,
                email: null,
                confirmEmail: null,
                isdCountryCodeId: null,
            },
            // accountInfo: {
            //     purposeOfActionId: null,
            //     products: [],
            //     expectedCredit: null,
            //     creditLimit: null,
            //     grossIncome: null,
            //     initialBalance: null,
            // },
            documentInfo: {
                utility: {
                    fileName: null,
                    originalFileName: null,
                },
                identity: {
                    fileName: null,
                    originalFileName: null,
                },
                video: {
                    fileName: null,
                    originalFileName: null,
                },
            },
            customInfo: {},
        })
        state.isIndustrial = true
        state.isTaxIdRequired = false
    },
    initCustomField(state, { key, value }) {
        Vue.set(state.form.customInfo, key, value)
    },
    updateField(state, payload) {
        _.set(state.form, payload.key, payload.value)
    },
    setValue(state, { key, value }) {
        state[key] = value
    },
}
