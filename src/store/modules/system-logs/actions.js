const apiUrl = process.env.VUE_APP_API_URL
import axios from 'axios'
const headers = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('vueauth_token')}`,
    },
}

export default {
    async exportLogs({ commit }, payload) {
        try {
            let { data } = await axios.post(
                apiUrl + 'system-logs/export',
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },

    async cancelExportProcess({ commit }, payload) {
        try {
            let { data } = await axios.post(
                apiUrl + 'system-logs/cancel-export',
                payload,
                headers
            )
            commit('setSuccess', data.message)
            commit('reloadList', true)
        } catch (errors) {
            commit(
                'setError',
                errors.response.data.errors[
                    Object.keys(errors.response.data.errors)[0]
                ]
            )
        }
    },
}
