import _ from 'lodash'

export default {
    systemLogsData: {
        //system logs page
        totalActions: {
            type: 'serial',
            categoryField: 'date',
            dataDateFormat: 'YYYY-MM-DD',
            pathToImages: 'https://www.amcharts.com/lib/3/images/',
            fontSize: 14,
            colors: [
                '#662D91',
                '#ff7217',
                '#B0DE09',
                '#0D8ECF',
                '#2A0CD0',
                '#CD0D74',
                '#CC0000',
                '#00CC00',
                '#0000CC',
                '#DDDDDD',
                '#999999',
                '#333333',
                '#990000',
            ],
            color: '#1B1B1E',
            fontFamily: 'ProximanovaRegular',
            theme: 'default',
            autoMargins: true,
            startDuration: 1,
            categoryAxis: {
                gridPosition: 'start',
                parseDates: true,
                gridColor: '#F2F2F6',
            },
            chartCursor: {
                enabled: true,
                categoryBalloonDateFormat: 'DD MMM YYYY',
                bulletSize: 6,
                cursorColor: '#B35FA5',
            },
            chartScrollbar: {
                enabled: false,
                backgroundColor: '#E8E8EF',
                graphFillColor: '#D7D7E2',
                graphLineColor: '#d7d7e2',
                selectedBackgroundColor: '#F2F2F6',
                tabIndex: 0,
            },
            trendLines: [],
            graphs: [
                {
                    fillAlphas: 1,
                    id: 'AmGraph-1',
                    title: 'Total actions',
                    type: 'column',
                    valueField: 'value',
                    balloonText: '[[title]]: [[value]]',
                },
            ],
            guides: [],
            valueAxes: [
                {
                    id: 'ValueAxis-1',
                    gridColor: '#E8E8EF',
                    gridAlpha: 1,
                    title: 'Total Actions',
                },
            ],
            allLabels: [],
            balloon: {
                horizontalPadding: 6,
                offsetX: 5,
                verticalPadding: 6,
                fillAlphas: 1,
            },
            titles: [],
        },
    },
    systemLogFields: [
        {
            key: 'user',
            label: 'User',
            sortable: false,
            formatter: (value) => {
                return _.startCase(value)
            },
        },
        {
            key: 'module',
            label: 'Module',
            sortable: true,
            formatter: (value) => {
                return _.startCase(value)
            },
        },
        {
            key: 'type',
            label: 'Action Type',
            sortable: true,
            formatter: (value) => {
                return _.startCase(value)
            },
        },
        {
            key: 'action',
            label: 'Action',
            sortable: true,
            // formatter: (value) => {
            //     return _.startCase(value)
            // },
        },
        {
            key: 'createdAt',
            label: 'Timestamp',
            sortable: true,
        },
    ],
    systemLogModules: [
        { text: 'Filter by module', value: null },
        { text: 'Authentication', value: 'authentication' },
        { text: 'System Users', value: 'system_users' },
        { text: 'Customers', value: 'customers' },
        { text: 'Alerts', value: 'alerts' },
        { text: 'Insights', value: 'insights' },
        { text: 'Form Builder', value: 'form_builder' },
        { text: 'Configurations', value: 'configuration' },
        { text: 'System Logs', value: 'system_logs' },
    ],

    systemLogActions: {
        authentication: [
            { text: 'Filter by action', value: null },
            { text: 'Login', value: 'logged_in' },
            { text: 'Contact Support', value: 'support_contacted' },
            { text: 'Set Password', value: 'set_password' },
            { text: 'Change Password', value: 'changed_password' },
            { text: 'Reset Password', value: 'requested_reset' },
            { text: 'Logout', value: 'logout' },
        ],
        system_users: [
            { text: 'Filter by action', value: null },
            { text: 'Listed System Users', value: 'list_viewed_users' },
            { text: 'Created System Users', value: 'created_user' },
            { text: 'Updated System User', value: 'updated_user' },
            { text: 'Archived System User', value: 'archived_user' },
            { text: 'Unarchived System User', value: 'unarchived_user' },
        ],
        access_control: [
            { text: 'Filter by action', value: null },
            { text: 'Listed Access Control', value: 'list_viewed_role' },
            { text: 'Created Access Control', value: 'created_role' },
            { text: 'Updated Access Control', value: 'updated_role' },
            { text: 'Archived Access Control', value: 'archived_role' },
            { text: 'Unarchived Access Control', value: 'unarchived_role' },
            { text: 'Listed Data Segment', value: 'list_viewed_data_segments' },
            { text: 'Created Data Segment', value: 'created_data_segment' },
            { text: 'Updated Data Segment', value: 'updated_data_segment' },
            { text: 'Archived Data Segment', value: 'archived_data_segment' },
            {
                text: 'Unarchived Data Segment',
                value: 'unarchived_data_segment',
            },
            {
                text: 'Listed Application Access',
                value: 'list_viewed_api_access',
            },
            {
                text: 'Created Application Access',
                value: 'created_api_access',
            },
            {
                text: 'Updated Application Access',
                value: 'updated_api_access',
            },
            {
                text: 'Archived Application Access',
                value: 'archived_api_access',
            },
            {
                text: 'Unarchived Application Access',
                value: 'unarchived_api_access',
            },
        ],
        customers: [
            { text: 'Filter by action', value: null },
            { text: 'Register F2F Customer', value: 'registration' },
            // {
            //     text: 'Register NF2F Customer',
            //     value: 'registered_non_face_to_face',
            // },
            // { text: 'List Customers', value: 'list_viewed_customer' },
            {
                text: 'Viewed Customer Profile',
                value: 'viewed-customer-profile',
            },
            { text: 'Updated Customer', value: 'updated_customer' },
            {
                text: 'Viewed Customer Name Screening',
                value: 'viewed_customer_name_screening',
            },
            {
                text: 'Viewed Customer Document Status',
                value: 'viewed_customer_document_status',
            },
            {
                text: 'Viewed Customer Risk Rating',
                value: 'viewed_customer_risk_rating',
            },
            {
                text: 'Viewed Customer Activities',
                value: 'viewed_customer_activities',
            },
            {
                text: 'Exported Customers',
                value: 'exported_customers',
            },
            {
                text: 'Exported Customers Activities',
                value: 'exported_customer_activities',
            },
        ],
        alerts: [
            { text: 'Filter by action', value: null },
            {
                text: 'Listed Alerts',
                value: 'list_viewed_alerts',
            },
            { text: 'Created Manual Alerts', value: 'created_alert' },
            { text: 'Initiated Review', value: 'lock_alert' },
            { text: 'KYC Status Review', value: 'approve_alert' },
            { text: 'Canceled Review', value: 'cancel_alert_review' },
            { text: 'Unlocked Alert', value: 'unlock_alert' },
        ],
        insights: [
            { text: 'Filter by action', value: null },
            {
                text: 'Exported Activity Dashboard Report excel',
                value: 'export-activity-dashboard-xlsx',
            },
            {
                text: 'Exported Customer Statistics Report pdf',
                value: 'export-customer-statistics-pdf',
            },
            {
                text: 'Exported Customer Statistics Report excel',
                value: 'export-customer-statistics-xlsx',
            },
            {
                text: 'Exported Activity Dashboard Report pdf',
                value: 'export-activity-dashboard-pdf',
            },
        ],
        form_builder: [
            { text: 'Filter by action', value: null },
            {
                text: 'Listed Form Builder',
                value: 'list_viewed_form_fields',
            },
            {
                text: 'Updated Form Builder Fields',
                value: 'updated_form_fields',
            },
        ],

        configuration: [
            { value: null, text: 'Filter by action' },
            { value: 'created_nationality', text: 'Created Nationality' },
            { value: 'updated_nationality', text: 'Updated Nationality' },
            { value: 'archived_nationality', text: 'Archived Nationality' },
            { value: 'unarchived_nationality', text: 'Unarchived Nationality' },
            { value: 'unarchived_product', text: 'Unarchived Product' },
            { value: 'created_state', text: 'Created State' },
            { value: 'updated_state', text: 'Updated State' },
            { value: 'archived_state', text: 'Archived State' },
            { value: 'unarchived_state', text: 'Unarchived State' },
            { value: 'created_country', text: 'Created Country' },
            { value: 'updated_country', text: 'Updated Country' },
            { value: 'archived_country', text: 'Archived Country' },
            { value: 'unarchived_country', text: 'Unarchived Country' },
            { value: 'created_work_type', text: 'Created Work Type' },
            { value: 'updated_work_type', text: 'Updated Work Type' },
            { value: 'archived_work_type', text: 'Archived Work Type' },
            { value: 'unarchived_work_type', text: 'Unarchived Work Type' },
            { value: 'created_industry', text: 'Created Industry' },
            { value: 'updated_industry', text: 'Updated Industry' },
            { value: 'archived_industry', text: 'Archived Industry' },
            { value: 'unarchived_industry', text: 'Unarchived Industry' },
            { value: 'created_product', text: 'Created Product' },
            { value: 'updated_product', text: 'Updated Product' },
            { value: 'archived_product', text: 'Archived Product' },

            {
                value: 'created_purpose_of_action',
                text: 'Created Purpose of Action',
            },
            {
                value: 'updated_purpose_of_action',
                text: 'Updated Purpose of Action',
            },
            {
                value: 'archived_purpose_of_action',
                text: 'Archived Purpose of Action',
            },
            {
                value: 'unarchived_purpose_of_action',
                text: 'Unarchived Purpose of Action',
            },
            { value: 'list_viewed_relationship', text: 'Listed Relationship' },
            { value: 'created_relationship', text: 'Created Relationship' },
            { value: 'updated_relationship', text: 'Updated Relationship' },
            { value: 'archived_relationship', text: 'Archived Relationship' },
            {
                value: 'unarchived_relationship',
                text: 'Unarchived Relationship',
            },
            { value: 'list_viewed_channel_type', text: 'Listed Channel Type' },
            { value: 'updated_channel_type', text: 'Updated Channel Type' },
            { value: 'list_viewed_risk_factor', text: 'Listed Risk Factor' },
            { value: 'updated_risk_factor', text: 'Updated Risk Factor' },
            {
                value: 'list_viewed_risk_rating_score',
                text: 'Listed Risk Rating Score',
            },
            {
                value: 'updated_risk_rating_score',
                text: 'Updated Risk Rating Score',
            },
            {
                value: 'list_viewed_document_verification_threshold',
                text: 'Listed Document Verification Threshold',
            },
            {
                value: 'updated_document_verification_threshold',
                text: 'Updated Document Verification Threshold',
            },
            {
                value: 'list_viewed_name_screening_scores',
                text: 'Listed Name Screening Scores',
            },
            {
                value: 'updated_name_screening_scores',
                text: 'Updated Name Screening Scores',
            },
            { value: 'viewed_brand_configurations', text: 'Viewed Branding' },
            { value: 'updated_brand_configurations', text: 'Updated Branding' },
            {
                value: 'updated_ocr_configurations',
                text: 'Modified On boarding Configurations',
            },
            {
                value: 'list_viewed_risk_level_review',
                text: 'Listed Risk Level Review',
            },
            {
                value: 'updated_risk_level_review',
                text: 'Updated Risk Level Review',
            },
            { value: 'viewed_aum_configurations', text: 'Viewed AUM' },
            { value: 'updated_aum_configurations', text: 'Updated AUM' },
            { value: 'viewed_sar_configurations', text: 'Viewed SAR' },
            { value: 'updated_sar_configurations', text: 'Updated SAR' },
            {
                value: 'initiated_simulation',
                text: 'Initiated Simulation',
            },
        ],

        computer_vision: [
            { text: 'Filter by action', value: null },
            { text: 'Parsed Image', value: 'parsed_image' },
            { text: 'Image Compared', value: 'image_compared' },
            { text: 'Liveness Detection', value: 'liveness_detected' },
        ],
        system_logs: [
            { text: 'Filter by action', value: null },
            { text: 'Viewed List System Logs', value: 'list_viewed_logs' },
            { text: 'Exported System Logs', value: 'exported_logs' },
        ],
        due_diligence: [
            { text: 'Filter by action', value: null },
            { text: 'Periodic Review', value: 'periodic_review' },
            { text: 'Customer Updated', value: 'customer_updated' },
            { text: 'Document Expiring', value: 'document_expiring' },
        ],
        screening_data: [
            { text: 'Filter by action', value: null },
            { text: 'Imported Data', value: 'imported_data' },
            {
                text: 'Viewed Import Statistics',
                value: 'viewed_import_statistics',
            },
            {
                text: 'Viewed List of Screening Configuration',
                value: 'list_viewed_screening_configuration',
            },
            {
                text: 'Updated Screening Configuration',
                value: 'updated_screening_configuration',
            },
        ],
    },
    totalRecords: null,
}
