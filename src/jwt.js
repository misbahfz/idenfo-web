const jwt = require('jsonwebtoken')
import store from '@/store/index'
import _ from 'lodash'
//let source
export default {
    async verify() {
        let res = false
        let token = store.getters.vueauth_token
        if (token) {
            try {
                let payload = await jwt.verify(
                    token,
                    process.env.VUE_APP_CLIENT_SECRET
                )
                if (payload) {
                    if (payload.scopes) {
                        let allowedRoles = [
                            'maker',
                            'checker',
                            'approver',
                            'unlock-alerts',
                        ]

                        /*** 
                            //Check if the user has any one of the above role
                        ***/
                        let kycRole = _.intersection(
                            payload.scopes,
                            allowedRoles
                        )
                        if (kycRole) {
                            store.commit('setKycRole', kycRole[0])
                        }
                        store.commit('setScopes', payload.scopes)
                    }
                    if (payload.userId) {
                        store.commit('setUserId', payload.userId)
                    }
                    res = true
                }
            } catch (error) {
                console.log(error)
                res = false
            }
        }
        return res
    },
}
