module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/recommended',
        'prettier',
        'prettier/vue',
        'plugin:prettier/recommended',
        '@vue/prettier',
    ],
    plugins: ['prettier'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    },
    parserOptions: {
        parser: 'babel-eslint',
    },
    globals: {
        _: 'readonly',
        AmCharts: 'readonly',
        httpRequest: 'readonly',
    },
}
